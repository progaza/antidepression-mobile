package kz.antiteam.antidepression;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.List;

import kz.antiteam.antidepression.fragment.BaseFragment;
import kz.antiteam.antidepression.fragment.chat.ConversationFragment;
import kz.antiteam.antidepression.fragment.chat.RoomFragment;
import kz.antiteam.antidepression.fragment.post.PostFragment;
import kz.antiteam.antidepression.fragment.registration.FirstPageFragment;
import kz.antiteam.antidepression.service.persistence.PersistentStorage;


public class AuthActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.auth_fragment_container, new FirstPageFragment())
                .commit();

    }

    @Override
    public void onBackPressed() {
        List<Fragment> fragmentList = getSupportFragmentManager().getFragments();

        boolean handled = false;
        for (Fragment f : fragmentList) {
            if (f instanceof BaseFragment) {
                handled = ((BaseFragment) f).onBackPressed();

                if (handled) {
                    break;
                }
            }
        }

        if (!handled) {
            super.onBackPressed();
        }
    }
}
