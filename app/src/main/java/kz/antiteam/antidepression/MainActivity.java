package kz.antiteam.antidepression;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.view.View;

import android.view.Menu;
import android.view.MenuItem;

import java.util.List;

import kz.antiteam.antidepression.fragment.BaseFragment;
import kz.antiteam.antidepression.fragment.category.AdminCategory;
import kz.antiteam.antidepression.fragment.chat.RoomFragment;
import kz.antiteam.antidepression.fragment.general.GeneralFragment;
import kz.antiteam.antidepression.fragment.post.CreatePostFragment;
import kz.antiteam.antidepression.fragment.post.PostFragment;
import kz.antiteam.antidepression.fragment.registration.FirstPageFragment;
import kz.antiteam.antidepression.service.persistence.PersistentStorage;
import kz.antiteam.antidepression.util.service.ServiceUtil;

public class MainActivity extends AppCompatActivity {
    private BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        PersistentStorage.init(getApplicationContext());

        bottomNavigationView = findViewById(R.id.bottom_nav_bar);
        bottomNavigationView.setOnNavigationItemSelectedListener(navListener);
        ServiceUtil.LOG("AM I NULL ? " + PersistentStorage.getStringProperty("username"));
        ServiceUtil.LOG("AM I NULL ? 2 " + PersistentStorage.getStringProperty("username").isEmpty());
        if (PersistentStorage.getStringProperty("username").isEmpty()) {

            Intent myIntent = new Intent(MainActivity.this, AuthActivity.class);
            MainActivity.this.startActivity(myIntent);
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.main_fragment_container, new PostFragment())
                    .commit();
        }


    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            menuItem -> {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                Fragment selectedFragment = null;
                Integer userId;
                switch (menuItem.getItemId()) {
                    case R.id.nav_home:
                        selectedFragment = new PostFragment();
                        break;
                    case R.id.nav_post:
                        selectedFragment = new CreatePostFragment();
                        break;
                    case R.id.nav_mail:
                        selectedFragment = new RoomFragment();
                        break;
                    case R.id.nav_settings:
                        selectedFragment = new GeneralFragment();
                        break;
//                    case R.id.nav_profile:
//                        selectedFragment = new PostFragment();
//                        break;
//                    case R.id.nav_profile:
//                        userId = PersistantStorage.getProperty("userId");
//                        if (userId == 0) {
//                            BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_bar);
//                            bottomNavigationView.getMenu().findItem(R.id.nav_home).setChecked(true);
//                            bottomNavigationView.getMenu().findItem(R.id.nav_profile).setChecked(false);
//                            Intent myIntent = new Intent(MainActivity.this, AuthActivity.class);
//                            MainActivity.this.startActivity(myIntent);
//                        } else {
//                            selectedFragment = new ProfileFragment();
//                        }
//                        break;
                    default:
                        selectedFragment = new PostFragment();
                        break;
                }
                if (selectedFragment != null)
                    fragmentManager
                            .beginTransaction()
                            .replace(R.id.main_fragment_container, selectedFragment)
                            .commit();
                return true;
            };

    @Override
    public void onBackPressed() {
        List<Fragment> fragmentList = getSupportFragmentManager().getFragments();

        boolean handled = false;
        for (Fragment f : fragmentList) {
            if (f instanceof BaseFragment) {
                handled = ((BaseFragment) f).onBackPressed();

                if (handled) {
                    break;
                }
            }
        }

        if (!handled) {
            super.onBackPressed();
        }
    }
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}