package kz.antiteam.antidepression.fragment.registration;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import kz.antiteam.antidepression.R;
import kz.antiteam.antidepression.service.persistence.PersistentStorage;
import kz.antiteam.antidepression.util.constant.Gender;

public class HiRegistrationFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hi_registration, container, false);
        Button maleButton = view.findViewById(R.id.auth_register_hi_male_button);
        Button femaleButton = view.findViewById(R.id.auth_register_hi_female_button);
        TextView backToFirstFragment = view.findViewById(R.id.auth_register_hi_back_to_first_fragment);
        maleButton.setOnClickListener(v -> {
            PersistentStorage.addProperty("gender", Gender.MALE.toString());
            goToPage(view, new AgeRegistrationFragment());
        });
        femaleButton.setOnClickListener(v -> {
            PersistentStorage.addProperty("gender", Gender.FEMALE.toString());
            goToPage(view, new AgeRegistrationFragment());
        });
        backToFirstFragment.setOnClickListener(v -> {
            goToPage(view, new FirstPageFragment());
        });
        return view;
    }

    private void goToPage(View view, Fragment fragment) {
        getParentFragmentManager()
                .beginTransaction()
                .replace(R.id.auth_fragment_container, fragment)
                .commit();
    }
}
