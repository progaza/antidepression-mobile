package kz.antiteam.antidepression.fragment.general;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.util.Collections;
import java.util.List;

import kz.antiteam.antidepression.MainActivity;
import kz.antiteam.antidepression.R;
import kz.antiteam.antidepression.adapter.PostAdapterRV;
import kz.antiteam.antidepression.fragment.BaseFragment;
import kz.antiteam.antidepression.fragment.post.PostFragment;
import kz.antiteam.antidepression.model.dto.PostDto;
import kz.antiteam.antidepression.service.ApiUtils;
import kz.antiteam.antidepression.service.persistence.PersistentStorage;
import kz.antiteam.antidepression.service.remote.IAssetService;
import kz.antiteam.antidepression.service.remote.IPostService;
import kz.antiteam.antidepression.util.service.ServiceUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyPostsFragment extends BaseFragment {

    private View view;
    private IPostService postService;
    private IAssetService assetService;
    private Gson gson;

    private RecyclerView recyclerView;
    private PostAdapterRV adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_my_posts, container, false);
        gson = new Gson();
        postService = ApiUtils.getPostService();
        assetService = ApiUtils.getAssetService();
        recyclerView = view.findViewById(R.id.rc_my_post_list);
        LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        getPosts(PersistentStorage.getIntProperty("userId").longValue());
        return view;
    }

    private void getPosts(Long userId) {
        postService.getPostDtoListByMe(userId).enqueue(new Callback<List<PostDto>>() {
            @Override
            public void onResponse(Call<List<PostDto>> call, Response<List<PostDto>> response) {
                if (response.isSuccessful()) {
                    ServiceUtil.LOG("RESPONSE " + gson.toJsonTree(response.body()));
                    List<PostDto> postDtos = response.body();
                    Collections.reverse(postDtos);
                    adapter = new PostAdapterRV(postDtos, getContext(), getFragmentManager(), assetService);
                    recyclerView.setAdapter(adapter);
                } else {
                    ServiceUtil.LOG("SUCCESS ERROR: " + gson.toJsonTree(response));
                    Toast.makeText(getContext(), getString(R.string.post_dto_get_error), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<PostDto>> call, Throwable t) {
                ServiceUtil.LOG("POST DTO LISTSUCCESS ERROR: " + gson.toJsonTree(t.getCause()));
                ServiceUtil.LOG("POST DTO LISTSUCCESS ERROR: " + gson.toJsonTree(t.getLocalizedMessage()));
                ServiceUtil.LOG("POST DTO LISTSUCCESS ERROR: " + gson.toJsonTree(t.fillInStackTrace()));
                Toast.makeText(getContext(), getString(R.string.post_dto_get_error), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onBackPressed() {
        backToPosts();
        return true;
    }

    public void backToPosts() {
        ((MainActivity) getContext())
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment_container, new GeneralFragment())
                .commit();
    }
}
