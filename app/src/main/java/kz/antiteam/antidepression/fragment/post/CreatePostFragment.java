package kz.antiteam.antidepression.fragment.post;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import kz.antiteam.antidepression.MainActivity;
import kz.antiteam.antidepression.R;
import kz.antiteam.antidepression.fragment.BaseFragment;
import kz.antiteam.antidepression.model.Asset;
import kz.antiteam.antidepression.model.PostComment;
import kz.antiteam.antidepression.model.dto.PostDto;
import kz.antiteam.antidepression.service.ApiUtils;
import kz.antiteam.antidepression.service.persistence.PersistentStorage;
import kz.antiteam.antidepression.service.remote.IAssetService;
import kz.antiteam.antidepression.service.remote.IPostService;
import kz.antiteam.antidepression.util.constant.Constant;
import kz.antiteam.antidepression.util.service.ImageUtils;
import kz.antiteam.antidepression.util.service.ServiceUtil;
import lombok.SneakyThrows;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class CreatePostFragment extends BaseFragment {
    private static int RESULT_LOAD_IMAGE = 1;
    private Gson gson;
    private IPostService postService;
    private IAssetService assetService;
    private View view;
    private ImageView imageView;
    private EditText postText;
    private Uri fileUri;
    private boolean hasImage = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_create_post, container, false);
        gson = new Gson();
        postService = ApiUtils.getPostService();
        assetService = ApiUtils.getAssetService();

        imageView = view.findViewById(R.id.create_post_photo);
        postText = view.findViewById(R.id.create_post_text);
        TextView buttonLoadImage = view.findViewById(R.id.create_post_select_photo);
        buttonLoadImage.setOnClickListener(arg0 -> {
            Intent i = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, RESULT_LOAD_IMAGE);
        });
        view.findViewById(R.id.create_post_button).setOnClickListener(arg0 -> {
            try {
                if (!postText.getText().toString().trim().isEmpty()) {
                    if (hasImage) {
                        upload();
                    } else {
                        createPost(null);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        return view;
    }

    public void upload() throws IOException {
        MediaType MEDIA_TYPE_JPEG = MediaType.parse("image/jpeg");
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        InputStream inputStream = getContext().getContentResolver().openInputStream(fileUri);
        ImageUtils.compress(inputStream, byteArrayOutputStream);

        RequestBody requestFile =
                RequestBody.create(
                        MEDIA_TYPE_JPEG,
                        byteArrayOutputStream.toByteArray()
                );

        MultipartBody.Part body =
                MultipartBody.Part.createFormData("asset", "AAAAAAAAA", requestFile);

        assetService.createAsset(body).enqueue(new Callback<Asset>() {
            @Override
            public void onResponse(Call<Asset> call, Response<Asset> response) {
                createPost(response.body().getId());
            }

            @Override
            public void onFailure(Call<Asset> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void createPost(Long assetId) {
        PostDto postDto = new PostDto();
        postDto.setBody(postText.getText().toString());
        postDto.setAssetId(assetId);
        postDto.setCreatorUserId(PersistentStorage.getIntProperty("userId").longValue());
        ServiceUtil.LOG("TO create: " + gson.toJsonTree(postDto));
        postService.createPost(postDto).enqueue(new Callback<PostDto>() {
            @Override
            public void onResponse(Call<PostDto> call, Response<PostDto> response) {
                ServiceUtil.LOG("CREATED : " + gson.toJsonTree(response.body()));
                backToPosts();
            }

            @Override
            public void onFailure(Call<PostDto> call, Throwable t) {
                ServiceUtil.showMessage(getContext(), "Something went wrong. Contact with admin");
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            fileUri = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContext().getContentResolver().query(fileUri,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            cursor.close();
            imageView.setImageURI(fileUri);
            hasImage = true;
        }
    }

    @Override
    public boolean onBackPressed() {
        backToPosts();
        return true;
    }

    public void backToPosts(){
        ((MainActivity) getContext())
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment_container, new PostFragment())
                .commit();
    }
}
