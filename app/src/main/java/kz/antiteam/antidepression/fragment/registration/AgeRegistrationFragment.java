package kz.antiteam.antidepression.fragment.registration;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import kz.antiteam.antidepression.R;
import kz.antiteam.antidepression.service.persistence.PersistentStorage;


public class AgeRegistrationFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_age_registration, container, false);
        CheckBox ageChecked = (CheckBox) view.findViewById(R.id.auth_register_age_checked);
        CheckBox nonAgeChecked = (CheckBox) view.findViewById(R.id.auth_register_non_age_checked);
        ageChecked.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                nonAgeChecked.setChecked(false);
                PersistentStorage.addProperty("age", 21);
            }
        });
        nonAgeChecked.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                ageChecked.setChecked(false);
                PersistentStorage.addProperty("age", 16);
            }
        });

        Button next = (Button) view.findViewById(R.id.auth_register_age_next_button);
        next.setOnClickListener(v -> {
            if (!ageChecked.isChecked() && !nonAgeChecked.isChecked()) {
                Toast.makeText(getContext(), getString(R.string.fill_all_fields), Toast.LENGTH_LONG).show();
            } else {
                getParentFragmentManager()
                        .beginTransaction()
                        .replace(R.id.auth_fragment_container, new RegistrationFragment())
                        .commit();
            }
        });

        TextView backToHi = view.findViewById(R.id.auth_register_age_back_to_hi);
        backToHi.setOnClickListener(v ->
                getParentFragmentManager()
                        .beginTransaction()
                        .replace(R.id.auth_fragment_container, new HiRegistrationFragment())
                        .commit());
        return view;
    }

}
