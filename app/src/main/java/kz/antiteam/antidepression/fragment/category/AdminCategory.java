package kz.antiteam.antidepression.fragment.category;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import kz.antiteam.antidepression.MainActivity;
import kz.antiteam.antidepression.R;
import kz.antiteam.antidepression.fragment.BaseFragment;
import kz.antiteam.antidepression.fragment.general.GeneralFragment;
import kz.antiteam.antidepression.fragment.post.PostFragment;

public class AdminCategory extends BaseFragment {

    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_admin_post, container, false);
        LinearLayout warningLayout = view.findViewById(R.id.category_warning_id);
        LinearLayout familyLayout = view.findViewById(R.id.category_family_id);
        LinearLayout relation = view.findViewById(R.id.category_relation_id);

        warningLayout.setOnClickListener((v) -> {
            String title = "Как научиться снимать тревогу?";
            String about = "Тревога — это естественный ответ организма на стрессовую ситуацию. Переживать о состоянии своего здоровья стоит в том случае, если подобное беспокойство тянется довольно продолжительное время и заметно отравляет вам жизнь. В таком случае принято говорить о хронической тревожности или генерализованном тревожном расстройстве.\n" +
                    "1. Задайте себе ряд вопросов\n" +
                    "2. Повторяйте ситуацию, приводящую к тревоге\n" +
                    "3. Запишите свои мысли \n" +
                    "4. Доведите беспокойство до абсурда \n" +
                    "5. Расслабьте мышцы \n" +
                    "6. Используйте диафрагмальное дыхание 7. Займитесь спортом \n" +
                    "8. Увеличьте повседневную активность \n" +
                    "9. Отвлекитесь на любимое хобби \n" +
                    "10. Послушайте любимую музыку \n" +
                    "11. Расчешите волосы \n" +
                    "12. Перекусите и попейте чая \n" +
                    "13. Займитесь уборкой \n" +
                    "14. Наполните пространство ароматами\n";
            String link = "https://youtu.be/1WfWRaeSZoU";
            ((MainActivity) getContext())
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.main_fragment_container, new CategoryPost(title, about, link))
                    .commit();
        });
        familyLayout.setOnClickListener((v) -> {
            String title = "Как подростку наладить отношения с семьёй?\n";
            String about = "Чтобы снова стать частью целого и вернуть себе расположение близких, предлагаем подросткам несколько простых советов, которых стоит придерживаться, чтобы добиться желаемого.\n" +
                    "Итак, как наладить отношения с мамой после разрыва или крупной ссоры?\n" +
                    "1. Во-первых, нужно научиться прощать. Если вы оба сможете отпустить обиды и избавиться от недомолвок, отношения наладятся.\n" +
                    "2. Во-вторых, поговорить по душам. Разговор скрепит семейные узы, позволит облегчить душу. Также вы сможете рассказать маме все, что накопилось за то время, пока вы не виделись, ведь мама – самый родной человек, который поддержит и посоветует что-то дельное в любой ситуации.\n" +
                    "3. В-третьих, больше времени проводить вместе. Позовите маму на совместный шопинг, прогуляйтесь по каким-нибудь местам, с которыми вас связывают общие воспоминания. Пусть они пробудят в вас тепло и детскую привязанность.\n" +
                    "4. Позвольте маме помочь вам. Поделитесь с ней своими проблемами, поговорите о планах и надеждах.\n" +
                    "Самое главное в попытках наладить отношения – искреннее желание, которое позволит подростку добиться желаемого и снова вернуть мир в семейный круг.\n";
            String link = "https://youtu.be/7E9ReycVJug";
            ((MainActivity) getContext())
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.main_fragment_container, new CategoryPost(title, about, link))
                    .commit();
        });
        relation.setOnClickListener((v) -> {
            String title = "Секрет идеальных отношений";
            String about = "Принцип 1. Общие ценности и цели.\n" +
                    "Без этого любые отношения могут потерпеть крушения так же, как самолет без системы навигации. Попробуйте прояснить, что для каждого из вас важно, ценно, а так же поставьте себе общие цели, если таких еще нету. \n" +
                    "\n" +
                    "Принцип 2. Ясность в отношениях: правила, принципы семьи/пары.\n" +
                    "Когда нет ясности и четких договоренностей, появляется ощущения опасности и тревожности. Более того, это приводит к разногласиям и конфликтам. Создайте понятные правила и принципы для вашей пары и увидите, насколько это упрощает жизнь.\n" +
                    "\n" +
                    "Принцип 3. Ответственность и взрослая позиция.\n" +
                    "Когда каждый в паре несет ответственность за то, что происходит в отношениях и каждый выбирает делать все возможное, чтобы отношения улучшались и развивались. Также, это уважение друг к другу и позиции «я ок - ты ок».\n" +
                    "\n" +
                    "Принцип 4. Снижайте уровень ожиданий от партнера.\n" +
                    "Никто никому ничего не должен в отношениях. Вы можете делать что-то или не делать в отношениях, но вы НЕ должны. \n" +
                    "\n" +
                    "Принцип 5. Работайте над отношениями, так как кризисы неизбежны.\n" +
                    "1 год, 3 года, 5, 7, 10 лет. В каждом из этих периодов отношения переходят на следующий этап, что зачастую сопровождается кризисами. Именно в эти периоды большинство пар расходиться. Зная эту закономерность, переживать их гораздо легче. А если вы еще и следуете предыдущим 4м принципам, тогда вам и вовсе нечего бояться.\n";
            String link = "https://youtu.be/ONPbCvlpKkg";
            ((MainActivity) getContext())
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.main_fragment_container, new CategoryPost(title, about, link))
                    .commit();
        });

        return view;
    }

    @Override
    public boolean onBackPressed() {
        backToPosts();
        return true;
    }

    public void backToPosts(){
        ((MainActivity) getContext())
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment_container, new GeneralFragment())
                .commit();
    }
}
