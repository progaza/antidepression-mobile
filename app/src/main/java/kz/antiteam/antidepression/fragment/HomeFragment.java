package kz.antiteam.antidepression.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {
//    private List<ProductDto> models;
//    private RecyclerView recyclerView;
//    private HomeProductRecyclerViewAdapter adapter;
//    private IProductService productService;
//    private Gson gson;
//    private LinearLayout morelayout;
//    private TextView moretext;
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_home, container, false);
//        this.recyclerView = view.findViewById(R.id.home_product_recycler_view);
//        this.gson = new Gson();
//        models = new ArrayList<>();
//        this.morelayout = view.findViewById(R.id.clickMoreDiv);
//        this.moretext = view.findViewById(R.id.clickMoreButton);
//
//        this.morelayout.setOnClickListener(v->{
//            goToAuctions();
//        });
//        this.moretext.setOnClickListener(v->{
//            goToAuctions();
//        });
//
//        this.productService = ApiUtils.getProductService();
//        getProductDtoList(view);
////        new CountDownTimer(30000, 1000) {
////
////            public void onTick(long millisUntilFinished) {
////                ServiceUtil.LOG("seconds remaining: " + millisUntilFinished / 1000);
////            }
////
////            public void onFinish() {
////                ServiceUtil.LOG("done!");
////            }
////        }.start();
//
//
//        return view;
//    }
//
//    private void goToAuctions() {
//
//        BottomNavigationView bottomNavigationView = (BottomNavigationView) getActivity().findViewById(R.id.bottom_nav_bar);
//        bottomNavigationView.getMenu().findItem(R.id.nav_home).setChecked(false);
//        bottomNavigationView.getMenu().findItem(R.id.nav_profile).setChecked(false);
//        bottomNavigationView.getMenu().findItem(R.id.nav_shop).setChecked(false);
//        bottomNavigationView.getMenu().findItem(R.id.nav_mail).setChecked(true);
//        getFragmentManager()
//                .beginTransaction()
//                .replace(R.id.main_fragment_container, new AuctionProductsFragment())
//                .commit();
//    }
//
//    private void getProductDtoList(View view) {
//        productService.getProductDtoAsAuctionIterable().enqueue(new Callback<ProductDtoResponseList>() {
//            @Override
//            public void onResponse(Call<ProductDtoResponseList> call, Response<ProductDtoResponseList> response) {
//
//                if (response.isSuccessful()) {
//                    ServiceUtil.LOG("SUCCESS RESPONSE: " + gson.toJsonTree(response));
//                    ProductDtoResponseList productDtoResponseList = response.body();
//                    if (productDtoResponseList.getStatus().equals("success")) {
//                        if (productDtoResponseList.getData() != null) {
//                            List<ProductDto> productDtoList = productDtoResponseList.getData();
//                            models = productDtoList;
//                            LinearLayoutManager linearLayoutManager =
//                                    new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
//                            recyclerView.setLayoutManager(linearLayoutManager);
//                            recyclerView.setItemAnimator(new DefaultItemAnimator());
//                            adapter = new HomeProductRecyclerViewAdapter(models, getContext(), getFragmentManager());
//                            recyclerView.setAdapter(adapter);
//                            final ProgressBar progressBar = view.findViewById(R.id.home_product_loader);
//                            progressBar.setVisibility(View.GONE);
//                        }
//                    }
//                    ServiceUtil.LOG("SUCCESS RESPONSE(V2): " + gson.toJsonTree(response.body()));
//                } else {
//                    ServiceUtil.LOG("NOT SUCCESS: " + gson.toJsonTree(response));
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ProductDtoResponseList> call, Throwable t) {
//                ServiceUtil.LOG("Error while getting ProductDtoResponseList in: " + t.getLocalizedMessage() + "\n-----------------" + t.getMessage());
//                ServiceUtil.LOG("Error while getting ProductDtoResponseList CAUSE: " + t.getCause());
//            }
//        });
//    }
}
