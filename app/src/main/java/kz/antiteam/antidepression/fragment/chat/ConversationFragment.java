package kz.antiteam.antidepression.fragment.chat;

import android.app.Service;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import kz.antiteam.antidepression.AuthActivity;
import kz.antiteam.antidepression.MainActivity;
import kz.antiteam.antidepression.R;
import kz.antiteam.antidepression.adapter.ConversationAdapterRV;
import kz.antiteam.antidepression.adapter.RoomAdapterRV;
import kz.antiteam.antidepression.fragment.BaseFragment;
import kz.antiteam.antidepression.model.Message;
import kz.antiteam.antidepression.model.Room;
import kz.antiteam.antidepression.service.persistence.PersistentStorage;
import kz.antiteam.antidepression.util.service.DateUtils;
import kz.antiteam.antidepression.util.service.ServiceUtil;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ConversationFragment extends BaseFragment {
    private static String COLLECTION_MESSAGE_KEY = "message";
    private static String COLLECTION_ROOM_KEY = "rooms";
    private static String ROOM_ID_KEY = "room_id";
    private CollectionReference messages;
    private CollectionReference rooms;
    private ListenerRegistration listenerRegistration;
    private Gson gson;
    private String participant;
    private Room room;
    private ConversationAdapterRV adapter;
    private RecyclerView recyclerView;
    private View view;
    private List<Message> messageList = new ArrayList<>();
    private TextView participantUsername;

    private ProgressBar progressBar;

    public ConversationFragment(String participant, Room room) {
        this.participant = participant;
        this.room = room;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_conversation, container, false);
        recyclerView = view.findViewById(R.id.recycler_gchat);
        progressBar = view.findViewById(R.id.conversation_loader);
        messages = FirebaseFirestore.getInstance().collection(COLLECTION_MESSAGE_KEY);
        rooms = FirebaseFirestore.getInstance().collection(COLLECTION_ROOM_KEY);
        gson = new Gson();

        LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        participantUsername = view.findViewById(R.id.conversation_participant);
        participantUsername.setText(this.participant);
//        messages.whereEqualTo(ROOM_ID_KEY, roomId).get()
//                .addOnCompleteListener((OnCompleteListener<QuerySnapshot>) val -> {
//                    addToCache(val);
//                    Collections.sort(messageList);
//                    ServiceUtil.LOG("SORTED MESSAGE LIST: " + gson.toJson(messageList));
//                    setModelsToRecyclerView(messageList);
//                });
        realtimeUpdateListener();
//        setModelsToRecyclerView(messageList);
        Button sendButton = view.findViewById(R.id.button_gchat_send);
        sendButton.setOnClickListener(this::sendMessage);
        return view;
    }

    private void sendMessage(View v) {
        EditText editText = ((EditText) view.findViewById(R.id.edit_gchat_message));
        String message = editText.getText().toString();
        if (message == null || message.isEmpty() || message.trim().isEmpty() || message.equals("")) {
            ServiceUtil.showMessage(getContext(), "Message cannot be empty!");
        } else {
            Message newMessage = new Message();
            newMessage.setOwner(PersistentStorage.getStringProperty("username"));
            newMessage.setRoomId(this.room.getDocId());
            newMessage.setText(message);
//            2021-04-11T12:48:25.872Z
            newMessage.setCreateDate(LocalDateTime.now().toString());
            editText.setText("");
            messages.add(newMessage.toHashMap())
                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            ServiceUtil.LOG("DocumentSnapshot written with ID: " + documentReference.getId());

                            documentReference.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                @Override
                                public void onSuccess(DocumentSnapshot documentSnapshot) {
                                    ServiceUtil.LOG("ADDED: " + gson.toJsonTree(documentSnapshot.getData()));
                                    updateRoom(newMessage);
                                }
                            });

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            e.printStackTrace();
                            ServiceUtil.LOG("Error adding document");
                        }
                    });
        }

    }

    private void updateRoom(Message newMessage) {
        HashMap<String, Object> toUpdateRoom = room.toHashMap(newMessage.getText());
        ServiceUtil.LOG("UPDATED ROOM: " + gson.toJsonTree(toUpdateRoom));
        rooms.document(room.getDocId()).set(toUpdateRoom)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        ServiceUtil.LOG("DocumentSnapshot successfully written!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        e.printStackTrace();
                        ServiceUtil.LOG("Error writing document");
                    }
                });
    }

    private void setModelsToRecyclerView(List<Message> roomList) {
//        messageList = fillListWithData(); FAKE DATA
        Collections.reverse(messageList);
        adapter = new ConversationAdapterRV(messageList, getContext(), getFragmentManager());
        recyclerView.setAdapter(adapter);
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    private void realtimeUpdateListener() {
        listenerRegistration = messages.whereEqualTo(ROOM_ID_KEY, room.getDocId())
                .addSnapshotListener((documentSnapshot, e) -> {
                    if (e != null) {
                        ServiceUtil.showMessage(getContext(), ("ERROR[firebase]: " + e.getLocalizedMessage()));
                    } else if (documentSnapshot != null && !documentSnapshot.isEmpty()) {
                        ServiceUtil.LOG("______________MESSAGES:______________");
                        List<Message> newMessageList = new ArrayList<>();
                        for (DocumentChange documentChange : documentSnapshot.getDocumentChanges()) {
                            newMessageList.add(new Message(documentChange.getDocument()));
                        }
                        ServiceUtil.LOG("NEW MESSAGES" + gson.toJson(newMessageList));
                        messageList.addAll(newMessageList);
                        Collections.sort(messageList);
//                        notifyCurrentRooms(updatedRooms);
                        setModelsToRecyclerView(messageList);
                    } else {

                        adapter = new ConversationAdapterRV(messageList, getContext(), getFragmentManager());
                        recyclerView.setAdapter(adapter);
                        progressBar.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                });
    }

    private List<Message> fillListWithData() {
        ArrayList<Message> messages = new ArrayList<>();
        Message message = new Message();
        message.setOwner("tester");
        message.setText("String use nemelo Lorem ips");
        messages.add(message);
        Message message1 = new Message();
        message1.setOwner("itantiteam@gmail.com");
        message1.setText("Hello asdlfa sudfh");
        messages.add(message1);
        Message message2 = new Message();
        message2.setOwner("tester");
        message2.setText("Flsytlalsdfuasd asldkf");
        messages.add(message2);
        return messages;
    }

    private void addToCache(Task<QuerySnapshot> task) {
        if (task.isSuccessful()) {
            List<DocumentSnapshot> documentList = task.getResult().getDocuments();
            documentList.forEach((val) -> {
                messageList.add(new Message(val));
            });
            ServiceUtil.LOG("MESSAGELIST 1: " + gson.toJson(messageList));
        } else {
            ServiceUtil.LOG("Firestore messages get failed: " + task.getException());
        }
    }

    @Override
    public boolean onBackPressed() {
        this.listenerRegistration.remove();
        ((MainActivity) getContext())
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment_container, new RoomFragment())
                .commit();
        return true;
    }
}
