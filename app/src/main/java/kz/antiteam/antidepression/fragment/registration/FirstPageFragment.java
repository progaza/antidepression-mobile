package kz.antiteam.antidepression.fragment.registration;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import kz.antiteam.antidepression.R;

public class FirstPageFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_first_page, container, false);

        final Button loginButton = view.findViewById(R.id.first_page_login);
        final Button registrationButton = view.findViewById(R.id.first_page_registration);

        loginButton.setOnClickListener(v -> getParentFragmentManager()
                .beginTransaction()
                .replace(R.id.auth_fragment_container, new LoginFragment())
                .commit());

        registrationButton.setOnClickListener(v -> getParentFragmentManager()
                .beginTransaction()
                .replace(R.id.auth_fragment_container, new HiRegistrationFragment())
                .commit());

        return view;
    }
}
