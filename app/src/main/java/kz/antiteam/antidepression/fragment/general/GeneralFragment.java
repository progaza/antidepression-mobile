package kz.antiteam.antidepression.fragment.general;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import kz.antiteam.antidepression.MainActivity;
import kz.antiteam.antidepression.R;
import kz.antiteam.antidepression.fragment.BaseFragment;
import kz.antiteam.antidepression.fragment.category.AdminCategory;
import kz.antiteam.antidepression.fragment.category.CategoryPost;

public class GeneralFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_general, container, false);
        LinearLayout profile = view.findViewById(R.id.general_profile_id);
        LinearLayout notifications = view.findViewById(R.id.general_notifications_id);
        LinearLayout myPosts = view.findViewById(R.id.general_my_posts_id);
        LinearLayout categories = view.findViewById(R.id.general_categories_id);

        profile.setOnClickListener((v) -> {
            ((MainActivity) getContext())
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.main_fragment_container, new ProfileFragment())
                    .commit();
        });
        notifications.setOnClickListener((v) -> {
            ((MainActivity) getContext())
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.main_fragment_container, new MyActionsFragment())
                    .commit();
        });
        myPosts.setOnClickListener((v) -> {
            ((MainActivity) getContext())
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.main_fragment_container, new MyPostsFragment())
                    .commit();
        });
        categories.setOnClickListener((v) -> {
            ((MainActivity) getContext())
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.main_fragment_container, new AdminCategory())
                    .commit();
        });
        // TODO front fix images, android finish fragmentation.....
        return view;
    }
}
