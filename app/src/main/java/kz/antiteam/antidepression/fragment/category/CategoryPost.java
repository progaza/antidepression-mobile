package kz.antiteam.antidepression.fragment.category;

import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import kz.antiteam.antidepression.MainActivity;
import kz.antiteam.antidepression.R;
import kz.antiteam.antidepression.fragment.BaseFragment;
import kz.antiteam.antidepression.fragment.chat.RoomFragment;

public class CategoryPost extends BaseFragment {

    private View view;
    private String title;
    private String about;
    private String link;

    public CategoryPost() {
    }

    public CategoryPost(String title, String about, String link) {
        this.title = title;
        this.about = about;
        this.link = link;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_category_post, container, false);
        TextView backPress = view.findViewById(R.id.back_to_admin_category);
        backPress.setOnClickListener((v) -> {
            this.onBackPressed();
        });
        TextView title = view.findViewById(R.id.category_title);
        title.setText(this.title);
        TextView about = view.findViewById(R.id.category_about);
        about.setText(this.about);
        TextView link = view.findViewById(R.id.category_link);
        SpannableString content = new SpannableString(this.link);
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        link.setText(content);
        return view;
    }

    @Override
    public boolean onBackPressed() {
        ((MainActivity) getContext())
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment_container, new AdminCategory())
                .commit();
        return true;
    }

}
