package kz.antiteam.antidepression.fragment.registration;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.gson.Gson;

import kz.antiteam.antidepression.R;
import kz.antiteam.antidepression.model.dto.UserRegistrationDto;
import kz.antiteam.antidepression.service.ApiUtils;
import kz.antiteam.antidepression.service.persistence.PersistentStorage;
import kz.antiteam.antidepression.service.remote.IAuthService;
import kz.antiteam.antidepression.util.constant.Gender;
import kz.antiteam.antidepression.util.service.ServiceUtil;
import lombok.SneakyThrows;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationFragment extends Fragment {

    private IAuthService authService;
    Button registerButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration, container, false);
        authService = ApiUtils.getAuthService();

        this.goToLoginRealizatison(view);
        this.actionRegisterRealization(view);

        return view;
    }

    private void goToLoginRealizatison(View view) {
        final TextView forwardToLogin = view.findViewById(R.id.auth_register_back_to_age);
        forwardToLogin.setOnClickListener(v -> getFragmentManager()
                .beginTransaction()
                .replace(R.id.auth_fragment_container, new AgeRegistrationFragment())
                .commit());
    }

    private void actionRegisterRealization(View view) {
        ///TODO add mail check

        registerButton = view.findViewById(R.id.auth_register_regbutton);
        final EditText fio = view.findViewById(R.id.auth_register_fio);
        final EditText mail = view.findViewById(R.id.auth_register_mail);
        final EditText password = view.findViewById(R.id.auth_register_password);

        registerButton.setOnClickListener(v -> {
            Integer ageStr = PersistentStorage.getIntProperty("age");
            Gender genderStr = Gender.valueOf(PersistentStorage.getStringProperty("gender"));
            String mailStr = mail.getText().toString();
            String passwordStr = password.getText().toString();
            String fioStr = fio.getText().toString();

            // global mistake marker
            boolean haveMistake = false;
            String errorString = "Ошибка. ";
            // check two passwords...
            if (
                    mailStr.isEmpty()
                            || passwordStr.isEmpty()
                            || fioStr.isEmpty()) {
                haveMistake = true;
                errorString += "Заполните все поля! ";
            }
            // extract names...

            if (!haveMistake) {
                if (ServiceUtil.isValidEmail(mailStr)) {
                    UserRegistrationDto userRegistrationDto
                            = new UserRegistrationDto(mailStr, fioStr, ageStr, genderStr, "", passwordStr);
                    registerButton.setEnabled(false);
                    registerWithApi(userRegistrationDto);
                } else {
                    ServiceUtil.showMessage(getContext(), getString(R.string.mail_is_not_valid));
                }

            } else {
                ServiceUtil.showMessage(getContext(), errorString);
            }

        });
    }

    private void registerWithApi(UserRegistrationDto userRegistrationDto) {
        Gson gson = new Gson();
        ServiceUtil.LOG("USER DATA FOR REG: " + gson.toJsonTree(userRegistrationDto));
        authService.register(userRegistrationDto).enqueue(new Callback<UserRegistrationDto>() {
            @SneakyThrows
            @Override
            public void onResponse(Call<UserRegistrationDto> call, Response<UserRegistrationDto> response) {
                registerButton.setEnabled(true);
                if (response.isSuccessful()) {
//                    ServiceUtil.LOG("SUCCESS RESPONSE: " + gson.toJsonTree(response));
                    ServiceUtil.LOG("SUCCESS RESPONSE : " + response.body());
                    Toast.makeText(getContext(), getString(R.string.auth_reg_success), Toast.LENGTH_LONG).show();

//                    Intent intent = new Intent(getContext(), MainActivity.class);
//                    intent.putExtra("userId", response.body().getId());
//                    getContext().startActivity(intent);
                } else {
                    ServiceUtil.LOG("HANDLED[mistake] RESPONSE: " + gson.toJsonTree(response));
                    ServiceUtil.LOG("HANDLED[mistake] RESPONSE: " + gson.toJsonTree(response.errorBody()));
                    Toast.makeText(getContext(), getString(R.string.registration_error), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<UserRegistrationDto> call, Throwable t) {
                ServiceUtil.LOG("HANDLED[error] RESPONSE: " + gson.toJsonTree(t.getCause()) + "\n-----------\n" + t.getLocalizedMessage());
                Toast.makeText(getContext(), getString(R.string.registration_error), Toast.LENGTH_LONG).show();
            }
        });
    }

}
