package kz.antiteam.antidepression.fragment.post;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.gson.Gson;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import kz.antiteam.antidepression.AuthActivity;
import kz.antiteam.antidepression.MainActivity;
import kz.antiteam.antidepression.R;
import kz.antiteam.antidepression.adapter.PostAdapterRV;
import kz.antiteam.antidepression.adapter.PostCommentAdapterRV;
import kz.antiteam.antidepression.fragment.BaseFragment;
import kz.antiteam.antidepression.fragment.chat.RoomFragment;
import kz.antiteam.antidepression.model.Message;
import kz.antiteam.antidepression.model.PostComment;
import kz.antiteam.antidepression.service.ApiUtils;
import kz.antiteam.antidepression.service.persistence.PersistentStorage;
import kz.antiteam.antidepression.service.remote.IPostCommentService;
import kz.antiteam.antidepression.util.service.ServiceUtil;
import lombok.SneakyThrows;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostCommentFragment extends BaseFragment {

    private IPostCommentService postCommentService;
    private Gson gson;
    private Long postId;
    private String title;
    private List<PostComment> postCommentList;


    private RecyclerView recyclerView;
    private PostCommentAdapterRV adapter;

    public PostCommentFragment() {
    }

    public PostCommentFragment(Long postId, String title) {
        this.postId = postId;
        this.title = title;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_post_comment, container, false);
        gson = new Gson();
        postCommentService = ApiUtils.getPostCommentService();
        recyclerView = view.findViewById(R.id.rc_post_comment_list);
        ((TextView) view.findViewById(R.id.post_comment_title)).setText("Публикация " + title);
        LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        Button sendMessage = view.findViewById(R.id.button_gchat_send);
        sendMessage.setOnClickListener((v) -> this.sendMessage(view));

        initList();
        return view;
    }

    private void sendMessage(View view) {
        EditText editText = ((EditText) view.findViewById(R.id.edit_gchat_message));
        String message = editText.getText().toString();
        if (message == null || message.isEmpty() || message.trim().isEmpty() || message.equals("")) {
            ServiceUtil.showMessage(getContext(), "Message cannot be empty!");
        } else {
            PostComment newComment = new PostComment();
            newComment.setUserId(Long.valueOf(PersistentStorage.getIntProperty("userId")));
            newComment.setText(message);
            newComment.setCreateDate(LocalDateTime.now().toString());
            newComment.setPostId(postId);
            editText.setText("");
            postCommentService.createPostComment(newComment).enqueue(new Callback<List<PostComment>>() {
                @Override
                public void onResponse(Call<List<PostComment>> call, Response<List<PostComment>> response) {
                    postCommentList = response.body();
                    Collections.reverse(postCommentList);
                    adapter = new PostCommentAdapterRV(postCommentList, getContext(), getFragmentManager(), postCommentService);
                    recyclerView.setAdapter(adapter);
                }

                @SneakyThrows
                @Override
                public void onFailure(Call<List<PostComment>> call, Throwable t) {
                    ServiceUtil.LOG("ERROR OF CREATE COMMENT: " + gson.toJsonTree(t.getCause()));
                    ServiceUtil.LOG("ERROR MESSAGE  OF CREATE COMMENT: " + gson.toJsonTree(t.getMessage()));
                    throw t;
                }
            });
        }
    }

    private void initList() {
        postCommentService.getListByPostId(this.postId).enqueue(new Callback<List<PostComment>>() {
            @Override
            public void onResponse(retrofit2.Call<List<PostComment>> call, retrofit2.Response<List<PostComment>> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        postCommentList = response.body();
                        Collections.reverse(postCommentList);
                        adapter = new PostCommentAdapterRV(postCommentList, getContext(), getFragmentManager(), postCommentService);
                        recyclerView.setAdapter(adapter);
                    }
                }
            }

            @SneakyThrows
            @Override
            public void onFailure(Call<List<PostComment>> call, Throwable t) {
                ServiceUtil.LOG("ERROR OF POSTCOMMENT: " + gson.toJsonTree(t.getCause()));
                ServiceUtil.LOG("ERROR MESSAGE OF POSTCOMMENT: " + gson.toJsonTree(t.getMessage()));
                throw t;
            }
        });
    }

    @Override
    public boolean onBackPressed() {
        ((MainActivity) getContext())
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment_container, new PostFragment())
                .commit();
        return true;
    }
}
