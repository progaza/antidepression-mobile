package kz.antiteam.antidepression.fragment.general;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.Gson;

import java.util.Collections;
import java.util.List;

import kz.antiteam.antidepression.MainActivity;
import kz.antiteam.antidepression.R;
import kz.antiteam.antidepression.adapter.PostAdapterRV;
import kz.antiteam.antidepression.fragment.BaseFragment;
import kz.antiteam.antidepression.model.User;
import kz.antiteam.antidepression.model.dto.PostDto;
import kz.antiteam.antidepression.model.dto.UserLoginDto;
import kz.antiteam.antidepression.service.ApiUtils;
import kz.antiteam.antidepression.service.persistence.PersistentStorage;
import kz.antiteam.antidepression.service.remote.IAuthService;
import kz.antiteam.antidepression.util.service.ServiceUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends BaseFragment {
    private View view;
    private IAuthService iAuthService;
    private Gson gson;
    private EditText name;
    private EditText email;
    private EditText age;
    private EditText about;
    private Button save;
    private User user;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_my_profile, container, false);
        this.gson = new Gson();
        this.name = view.findViewById(R.id.profile_name);
        this.email = view.findViewById(R.id.profile_email);
        this.age = view.findViewById(R.id.profile_age);
        this.about = view.findViewById(R.id.profile_about);
        this.save = view.findViewById(R.id.profile_save_button);
        this.iAuthService = ApiUtils.getAuthService();

        getUser(PersistentStorage.getIntProperty("userId").longValue());

        this.save.setOnClickListener(v -> {
            user.setAbout(about.getText().toString());
            user.setAge(Integer.parseInt(age.getText().toString()));
            user.setName(name.getText().toString());
            save(user);
        });
        return view;
    }

    private void save(User user) {
        iAuthService.update(user).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {

                    Toast.makeText(getContext(), "Ваши данные сохранены успешно!", Toast.LENGTH_LONG).show();
                } else {
                    ServiceUtil.LOG("SUCCESS ERROR: " + gson.toJsonTree(response));
                    Toast.makeText(getContext(), getString(R.string.login_or_password_incorrect), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                ServiceUtil.LOG("SUCCESS ERROR: " + gson.toJsonTree(t.getCause()));
                ServiceUtil.LOG("SUCCESS ERROR: " + gson.toJsonTree(t.getLocalizedMessage()));
                ServiceUtil.LOG("SUCCESS ERROR: " + gson.toJsonTree(t.fillInStackTrace()));
                Toast.makeText(getContext(), getString(R.string.auth_error), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getUser(Long userId) {
        iAuthService.getPersonData(userId).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    ServiceUtil.LOG("RESPONSE " + gson.toJsonTree(response.body()));
                    user = response.body();
                    name.setText(user.getName());
                    email.setText(user.getEmail());
                    age.setText(user.getAge().toString());
                    about.setText(user.getAbout());
                } else {
                    ServiceUtil.LOG("SUCCESS ERROR: " + gson.toJsonTree(response));
                    Toast.makeText(getContext(), getString(R.string.post_dto_get_error), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                ServiceUtil.LOG("POST DTO LISTSUCCESS ERROR: " + gson.toJsonTree(t.getCause()));
                ServiceUtil.LOG("POST DTO LISTSUCCESS ERROR: " + gson.toJsonTree(t.getLocalizedMessage()));
                ServiceUtil.LOG("POST DTO LISTSUCCESS ERROR: " + gson.toJsonTree(t.fillInStackTrace()));
                Toast.makeText(getContext(), getString(R.string.post_dto_get_error), Toast.LENGTH_LONG).show();
            }
        });
    }


    @Override
    public boolean onBackPressed() {
        backToPosts();
        return true;
    }

    public void backToPosts() {
        ((MainActivity) getContext())
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment_container, new GeneralFragment())
                .commit();
    }
}
