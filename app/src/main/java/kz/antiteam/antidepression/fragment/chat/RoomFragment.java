package kz.antiteam.antidepression.fragment.chat;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import kz.antiteam.antidepression.AuthActivity;
import kz.antiteam.antidepression.R;
import kz.antiteam.antidepression.adapter.RoomAdapterRV;
import kz.antiteam.antidepression.fragment.BaseFragment;
import kz.antiteam.antidepression.model.Room;
import kz.antiteam.antidepression.service.persistence.PersistentStorage;
import kz.antiteam.antidepression.util.service.ServiceUtil;

public class RoomFragment extends BaseFragment {
    private static String COLLECTION_KEY = "rooms";

    private CollectionReference rooms;
    private String email;
    private HashMap<String, Room> roomList;
    private ListenerRegistration listenerRegistration;
    private Gson gson = new Gson();

    private ProgressBar progressBar;
    private LinearLayout linearLayout;
    private RecyclerView recyclerView;
    private RoomAdapterRV adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        progressBar = view.findViewById(R.id.room_loader);
        recyclerView = view.findViewById(R.id.room_rv_room);
        linearLayout = view.findViewById(R.id.room_header);

        email = PersistentStorage.getStringProperty("username");
        rooms = FirebaseFirestore.getInstance().collection(COLLECTION_KEY);
        roomList = new HashMap<>();

        rooms.whereEqualTo("owner", email).get().addOnCompleteListener((OnCompleteListener<QuerySnapshot>) val -> {
            addToCache(val);
            rooms.whereEqualTo("participant", email).get().addOnCompleteListener((OnCompleteListener<QuerySnapshot>) val2 -> {
                addToCache(val2);
                LinearLayoutManager linearLayoutManager =
                        new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, true);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());

                ArrayList<Room> readyRoomList = new ArrayList(roomList.values());
                Collections.sort(readyRoomList);
                ServiceUtil.LOG("SORTED LIST: " + gson.toJson(readyRoomList));
                setModelsToRecyclerView(readyRoomList);
            });
        });
        realtimeUpdateListener();

        return view;
    }

    private void setModelsToRecyclerView(List<Room> roomList) {
//        productDtoList = fillListWithData(productDtoList);
        adapter = new RoomAdapterRV(roomList, getContext(), getFragmentManager());
        recyclerView.setAdapter(adapter);

        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.listenerRegistration.remove();
    }

    private void addToCache(Task<QuerySnapshot> task) {
        if (task.isSuccessful()) {
            List<DocumentSnapshot> documentList = task.getResult().getDocuments();
            documentList.forEach((val) -> {
                roomList.put(val.getId(), new Room(val));
            });
            ServiceUtil.LOG("ROOMLIST 1: " + gson.toJson(roomList));
        } else {
            ServiceUtil.LOG("Firestore rooms get participant failed: " + task.getException());
        }
    }

    private void realtimeUpdateListener() {
        listenerRegistration = rooms.addSnapshotListener((documentSnapshot, e) -> {
            if (e != null) {
                ServiceUtil.showMessage(getContext(), ("ERROR[firebase]: " + e.getLocalizedMessage()));
            } else if (documentSnapshot != null && !documentSnapshot.isEmpty()) {
                ServiceUtil.LOG("______________ROOMS:______________");
                HashMap<String, Room> updatedRooms = new HashMap<>();
                for (DocumentChange documentChange : documentSnapshot.getDocumentChanges()) {
                    updatedRooms.put(documentChange.getDocument().getId(), new Room(documentChange.getDocument()));
                }
                ServiceUtil.LOG(gson.toJson(updatedRooms));
                updatedRooms.forEach((docId, room) -> {
                    roomList.put(docId, room);
                });
                ArrayList<Room> readyRoomList = new ArrayList(roomList.values());
                Collections.sort(readyRoomList);
                ServiceUtil.LOG("SORTED LIST: " + gson.toJson(readyRoomList));
                setModelsToRecyclerView(readyRoomList);
            }
        });
    }

    private void notifyCurrentRooms(HashMap<String, Room> updatedRooms) {
        updatedRooms.forEach((docId, room) -> {
            roomList.put(docId, room);
        });
        ServiceUtil.LOG("SIZE: " + roomList.size());
        ServiceUtil.LOG("ROOMLIST: " + gson.toJson(roomList));
    }

    @Override
    public boolean onBackPressed() {
        listenerRegistration.remove();
        return false;
    }
}

