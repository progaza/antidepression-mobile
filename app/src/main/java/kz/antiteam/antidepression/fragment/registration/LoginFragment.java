package kz.antiteam.antidepression.fragment.registration;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;

import kz.antiteam.antidepression.MainActivity;
import kz.antiteam.antidepression.R;
import kz.antiteam.antidepression.fragment.registration.HiRegistrationFragment;
import kz.antiteam.antidepression.fragment.registration.RegistrationFragment;
import kz.antiteam.antidepression.model.User;
import kz.antiteam.antidepression.model.dto.UserLoginDto;
import kz.antiteam.antidepression.service.ApiUtils;
import kz.antiteam.antidepression.service.persistence.PersistentStorage;
import kz.antiteam.antidepression.service.remote.IAuthService;
import kz.antiteam.antidepression.util.service.ServiceUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginFragment extends Fragment {

    private IAuthService authService;
    private Gson gson;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        this.authService = ApiUtils.getAuthService();
        this.gson = new Gson();

        final Button loginButton = view.findViewById(R.id.auth_login_button);
        final EditText username = view.findViewById(R.id.auth_login_mail);
        final EditText password = view.findViewById(R.id.auth_login_password);

        loginButton.setOnClickListener(v -> {
            String usernameStr = username.getText().toString();
            String passwordStr = password.getText().toString();

            if (!usernameStr.trim().equals("") && !passwordStr.trim().equals("")) {
                if (ServiceUtil.isValidEmail(usernameStr)) {
                    loginWithApi(usernameStr, passwordStr);
                } else {
                    ServiceUtil.showMessage(getContext(), getString(R.string.mail_is_not_valid));
                }
            } else {
                ServiceUtil.showMessage(getContext(), getString(R.string.fill_all_fields));
            }

        });

        final TextView forwardButton = view.findViewById(R.id.forward_to_registration);

        forwardButton.setOnClickListener(v -> getParentFragmentManager()
                .beginTransaction()
                .replace(R.id.auth_fragment_container, new HiRegistrationFragment())
                .commit());
        return view;
    }


    private void loginWithApi(String username, String password) {
        UserLoginDto userLoginDto =
                new UserLoginDto(username, password);
        ServiceUtil.LOG("USER DATA: " + gson.toJsonTree(userLoginDto));
        authService.login(userLoginDto).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {

                    ServiceUtil.LOG("SUCCESS RESPONSE: " + gson.toJsonTree(response));
                    Toast.makeText(getContext(), getString(R.string.auth_login_success), Toast.LENGTH_LONG).show();
                    PersistentStorage.addProperty("username", username);
                    PersistentStorage.addProperty("userId", response.body().getId().intValue());
                    Intent intent = new Intent(getContext(), MainActivity.class);
                    intent.putExtra("userId", response.body().getId());
                    getContext().startActivity(intent);
                } else {
                    ServiceUtil.LOG("SUCCESS ERROR: " + gson.toJsonTree(response));
                    Toast.makeText(getContext(), getString(R.string.login_or_password_incorrect), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                ServiceUtil.LOG("SUCCESS ERROR: " + gson.toJsonTree(t.getCause()));
                ServiceUtil.LOG("SUCCESS ERROR: " + gson.toJsonTree(t.getLocalizedMessage()));
                ServiceUtil.LOG("SUCCESS ERROR: " + gson.toJsonTree(t.fillInStackTrace()));
                Toast.makeText(getContext(), getString(R.string.auth_error), Toast.LENGTH_LONG).show();
            }
        });
    }
}
