package kz.antiteam.antidepression.adapter;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.util.List;

import kz.antiteam.antidepression.AuthActivity;
import kz.antiteam.antidepression.MainActivity;
import kz.antiteam.antidepression.R;
import kz.antiteam.antidepression.fragment.chat.ConversationFragment;
import kz.antiteam.antidepression.model.Room;
import kz.antiteam.antidepression.service.persistence.PersistentStorage;
import kz.antiteam.antidepression.util.service.ServiceUtil;
import lombok.val;

public class RoomAdapterRV extends RecyclerView.Adapter<RoomAdapterRV.ViewHolder> {
    private List<Room> roomList;
    private Context context;
    private FragmentManager fragmentManager;

    public RoomAdapterRV(List<Room> roomList, Context context, FragmentManager fragmentManager) {
        this.roomList = roomList;
        this.context = context;
        this.fragmentManager = fragmentManager;
    }

    @NonNull
    @Override
    public RoomAdapterRV.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_room, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RoomAdapterRV.ViewHolder holder, int position) {
        Room room = roomList.get(position);
        holder.roomId.setText(room.getDocId());
        holder.date.setText(room.getCreateDate());
        holder.lastMessage.setText(room.getLastMessage());
        String title = PersistentStorage.getStringProperty("username").equalsIgnoreCase(room.getOwner()) ? room.getParticipant() : room.getOwner();
        holder.title.setText(title);
    }

    @Override
    public int getItemCount() {
        return this.roomList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView roomId;
        TextView title;
        TextView date;
        TextView lastMessage;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            roomId = itemView.findViewById(R.id.rv_room_id);
            title = itemView.findViewById(R.id.rv_room_title);
            date = itemView.findViewById(R.id.rv_room_date);
            lastMessage = itemView.findViewById(R.id.rv_room_last_message);
            title.setOnClickListener(this::onClick);
            date.setOnClickListener(this::onClick);
            lastMessage.setOnClickListener(this::onClick);

        }

        private void onClick(View v) {
            val productDetailFragment = new ConversationFragment(
                    title.getText().toString(),
                    roomList.stream().filter(val -> val.getDocId().contentEquals(roomId.getText())).findFirst().get());
            ServiceUtil.LOG("RoomId : " + roomId.getText());
            ((MainActivity) context)
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.main_fragment_container, productDetailFragment)
                    .commit();
        }

    }
}