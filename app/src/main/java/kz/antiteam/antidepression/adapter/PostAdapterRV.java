package kz.antiteam.antidepression.adapter;

import android.app.Service;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import kz.antiteam.antidepression.AuthActivity;
import kz.antiteam.antidepression.MainActivity;
import kz.antiteam.antidepression.R;
import kz.antiteam.antidepression.fragment.chat.ConversationFragment;
import kz.antiteam.antidepression.fragment.post.PostCommentFragment;
import kz.antiteam.antidepression.model.Room;
import kz.antiteam.antidepression.model.dto.PostDto;
import kz.antiteam.antidepression.service.persistence.PersistentStorage;
import kz.antiteam.antidepression.service.remote.IAssetService;
import kz.antiteam.antidepression.util.service.DateUtils;
import kz.antiteam.antidepression.util.service.ServiceUtil;
import okhttp3.ResponseBody;
import retrofit2.Callback;

public class PostAdapterRV extends RecyclerView.Adapter<PostAdapterRV.ViewHolder> {
    private static String COLLECTION_KEY = "rooms";

    private List<PostDto> postDtoList;
    private Context context;
    private FragmentManager fragmentManager;
    private IAssetService iAssetService;
    private HashMap<Long, ResponseBody> responseBodyHashMap;
    private CollectionReference rooms;

    public PostAdapterRV(List<PostDto> postDtoList, Context context, FragmentManager fragmentManager, IAssetService iAssetService) {
        this.postDtoList = postDtoList;
        this.context = context;
        this.fragmentManager = fragmentManager;
        this.iAssetService = iAssetService;
        this.responseBodyHashMap = new HashMap<>();
        rooms = FirebaseFirestore.getInstance().collection(COLLECTION_KEY);
    }

    @NonNull
    @Override
    public PostAdapterRV.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_post, parent, false);
        return new PostAdapterRV.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PostAdapterRV.ViewHolder holder, int position) {
        PostDto postDto = postDtoList.get(position);
        holder.id.setText(String.valueOf(postDto.getId()));
        holder.owner.setText(postDto.getUserName()); // TODO set user email
        holder.body.setText(postDto.getBody());
        holder.commentCount.setText(Html.fromHtml(String.format("<b><u>Комментарии(%s)</u></b>", String.valueOf(postDto.getCommentCount()))));

        String datee = postDto.getCreateDate();
        if (!datee.endsWith("Z")) {
            datee += "Z";
        }
        if (datee.length() > 18) {
            ServiceUtil.LOG("cur : " + datee);
            int l = 5;
            if(datee.length() == 23){
                l = 4;
            }else if(datee.length() == 27){
                l = 8;
            }
            int until = datee.length() - l;
            ServiceUtil.LOG("cur unt: " + until);

            String substring = datee.replace("T", " ").substring(0, until);
            holder.createDate.setText(DateUtils.stringDateToFormatString(substring, "dd.MM HH:mm"));
        }
        iAssetService.getImageById(postDto.getAssetId()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Bitmap bmp = BitmapFactory.decodeStream(response.body().byteStream());
                        holder.imageView.setImageBitmap(bmp);
                        holder.imageView.setVisibility(View.VISIBLE);
                        responseBodyHashMap.put(postDto.getAssetId(), response.body());
                    } else {
//                        holder.imageView.setImageResource(R.drawable.card_example_2);
                    }
                } else {
//                    holder.imageView.setImageResource(R.drawable.card_example_2);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {
//                holder.imageView.setImageResource(R.drawable.card_example_2);
            }
        });
        iAssetService.getImageById(postDto.getUserAssetId()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Bitmap bmp = BitmapFactory.decodeStream(response.body().byteStream());
                        holder.avatarView.setImageBitmap(bmp);
                        holder.avatarView.setVisibility(View.VISIBLE);
                        responseBodyHashMap.put(postDto.getUserAssetId(), response.body());
                    } else {
//                        holder.imageView.setImageResource(R.drawable.card_example_2);
                    }
                } else {
//                    holder.imageView.setImageResource(R.drawable.card_example_2);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {
//                holder.imageView.setImageResource(R.drawable.card_example_2);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.postDtoList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        ImageView avatarView;
        TextView id;
        TextView owner;
        TextView body;
        TextView commentCount;
        TextView createDate;
        TextView chat;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.rc_post_id);
            owner = itemView.findViewById(R.id.rc_post_owner);
            body = itemView.findViewById(R.id.rc_post_body);
            commentCount = itemView.findViewById(R.id.rc_post_comment_count);
            createDate = itemView.findViewById(R.id.rc_post_create_date);
            imageView = itemView.findViewById(R.id.rc_post_image);
            avatarView = itemView.findViewById(R.id.rc_post_avatar);
            chat = itemView.findViewById(R.id.rc_post_comment_chat);

            commentCount.setOnClickListener((v) -> {
                ServiceUtil.LOG("post id : " + id.getText());
                ((MainActivity) context)
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.main_fragment_container, new PostCommentFragment(Long.valueOf((String) id.getText()), (String) owner.getText()))
                        .commit();
            });
            chat.setOnClickListener((v) -> {
                ServiceUtil.LOG("post id : " + id.getText());
                // TODO create if not exist room
                rooms.document("it2CLLnpgiWfNNEcqxyJ").get().addOnCompleteListener(val -> {
                    if (val.isSuccessful()) {
                        DocumentSnapshot documentList = val.getResult();
                        ServiceUtil.LOG("FOUND: " + documentList);
                        ((MainActivity) context)
                                .getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.main_fragment_container, new ConversationFragment((String) owner.getText(), new Room(documentList)))
                                .commit();
                    } else {
                        ServiceUtil.LOG("Firestore rooms get participant failed: " + val.getException());
                    }
                });
            });
        }

    }
}