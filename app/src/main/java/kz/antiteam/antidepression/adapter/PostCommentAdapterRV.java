package kz.antiteam.antidepression.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import kz.antiteam.antidepression.R;
import kz.antiteam.antidepression.model.PostComment;
import kz.antiteam.antidepression.model.dto.NotificationDTO;
import kz.antiteam.antidepression.service.remote.IPostCommentService;

public class PostCommentAdapterRV extends RecyclerView.Adapter<PostCommentAdapterRV.ViewHolder> {
    private List<PostComment> postCommentList;
    private Context context;
    private FragmentManager fragmentManager;
    private IPostCommentService postCommentService;

    public PostCommentAdapterRV(List<PostComment> postCommentList, Context context, FragmentManager fragmentManager, IPostCommentService postCommentService) {
        this.postCommentList = postCommentList;
        this.context = context;
        this.fragmentManager = fragmentManager;
        this.postCommentService = postCommentService;
    }

    public PostCommentAdapterRV(List<NotificationDTO> postCommentList, Context context, FragmentManager fragmentManager, IPostCommentService postCommentService, String val) {
        this.postCommentList = new ArrayList<>();
        for (NotificationDTO vals : postCommentList) {
            this.postCommentList.add(new PostComment(vals));
        }
        this.context = context;
        this.fragmentManager = fragmentManager;
        this.postCommentService = postCommentService;
    }

    @NonNull
    @Override
    public PostCommentAdapterRV.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_post_comment, parent, false);
        return new PostCommentAdapterRV.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PostCommentAdapterRV.ViewHolder holder, int position) {
        PostComment postComment = postCommentList.get(position);
        holder.userName.setText(postComment.getUserName());
        holder.text.setText(postComment.getText());
        holder.createDate.setText(postComment.getLocalDate());
    }

    @Override
    public int getItemCount() {
        return this.postCommentList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView userName;
        TextView text;
        TextView createDate;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            userName = itemView.findViewById(R.id.rc_post_comment_user_name);
            text = itemView.findViewById(R.id.rc_post_comment_text);
            createDate = itemView.findViewById(R.id.rc_post_comment_create_date);
        }

    }
}
