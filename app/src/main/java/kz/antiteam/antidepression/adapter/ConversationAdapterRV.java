package kz.antiteam.antidepression.adapter;

import android.content.Context;
import android.os.storage.StorageManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import org.w3c.dom.Text;

import java.util.List;

import kz.antiteam.antidepression.AuthActivity;
import kz.antiteam.antidepression.R;
import kz.antiteam.antidepression.fragment.chat.ConversationFragment;
import kz.antiteam.antidepression.model.Message;
import kz.antiteam.antidepression.model.Room;
import kz.antiteam.antidepression.service.persistence.PersistentStorage;
import kz.antiteam.antidepression.util.service.ServiceUtil;

public class ConversationAdapterRV extends RecyclerView.Adapter<ConversationAdapterRV.ViewHolder> {
    private List<Message> messageList;
    private Context context;
    private FragmentManager fragmentManager;

    public ConversationAdapterRV(List<Message> messageList, Context context, FragmentManager fragmentManager) {
        this.messageList = messageList;
        ServiceUtil.LOG("MESSAGES : " + new Gson().toJson(messageList));
        this.context = context;
        this.fragmentManager = fragmentManager;
    }

    @NonNull
    @Override
    public ConversationAdapterRV.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_conversation, parent, false);
        return new ConversationAdapterRV.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ConversationAdapterRV.ViewHolder holder, int position) {
        Message message = messageList.get(position);
        holder.text.setText(message.getText());
//        holder.title.setText(room.getOwner());
//        holder.date.setText(room.getCreateDate());
        String username = PersistentStorage.getStringProperty("username");
        if (username.equalsIgnoreCase(message.getOwner())) {
            holder.messageHelper.setVisibility(View.VISIBLE);
            holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorOwner));
            holder.cardView.setRadius(15);
            holder.cardView.setElevation(0);
            holder.cardView.setPreventCornerOverlap(false);
            holder.cardView.setUseCompatPadding(true);
        }
    }

    @Override
    public int getItemCount() {
        return this.messageList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView messageHelper;
        TextView text;
        TextView time;
        TextView date;
        TextView lastMessage;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.card_gchat_message);
            messageHelper = itemView.findViewById(R.id.chat_message_helper);
            text = itemView.findViewById(R.id.text_gchat_message_me);
            time = itemView.findViewById(R.id.text_gchat_timestamp_me);
        }

    }
}