package kz.antiteam.antidepression.service;


import kz.antiteam.antidepression.service.remote.IAssetService;
import kz.antiteam.antidepression.service.remote.IAuthService;
import kz.antiteam.antidepression.service.remote.IPostCommentService;
import kz.antiteam.antidepression.service.remote.IPostService;
import kz.antiteam.antidepression.service.remote.RetrofitClient;
import kz.antiteam.antidepression.util.constant.Constant;

public class ApiUtils {

    private ApiUtils() {
    }

    public static final String BASE_URL = Constant.BASE_URL;

    public static IAuthService getAuthService() {
        return RetrofitClient.getClient(BASE_URL).create(IAuthService.class);
    }

    public static IPostService getPostService() {
        return RetrofitClient.getClient(BASE_URL).create(IPostService.class);
    }

    public static IAssetService getAssetService() {
        return RetrofitClient.getClient(BASE_URL).create(IAssetService.class);
    }

    public static IPostCommentService getPostCommentService() {
        return RetrofitClient.getClient(BASE_URL).create(IPostCommentService.class);
    }

}