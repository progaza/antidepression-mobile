package kz.antiteam.antidepression.service.remote;

import io.reactivex.Single;
import kz.antiteam.antidepression.model.Asset;
import kz.antiteam.antidepression.model.User;
import kz.antiteam.antidepression.model.dto.UserLoginDto;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Url;

public interface IAssetService {
    @GET("")
    Single<User> getPersonData(@Path("userId") Integer userId);

    //File file = // initialize file here
    //
    //MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
    //
    //Call<MyResponse> call = api.uploadAttachment(filePart);

    @POST("/asset")
    @Multipart
    Call<Asset> createAsset(@Part MultipartBody.Part filePart);

    @GET("/asset/{id}")
    Call<ResponseBody> getImageById(@Path("id") Long id);
}
