package kz.antiteam.antidepression.service.remote;

import kz.antiteam.antidepression.model.User;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface IUserService {
    @GET("/limbo/core-service/user/read/{id}")
    Call<User> getUserById(@Path("id") Integer userId);

}
