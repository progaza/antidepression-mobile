package kz.antiteam.antidepression.service.remote;

import io.reactivex.Single;
import kz.antiteam.antidepression.model.User;
import kz.antiteam.antidepression.model.dto.UserLoginDto;
import kz.antiteam.antidepression.model.dto.UserRegistrationDto;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface IAuthService {
    @GET("/current/{userId}")
    Call<User> getPersonData(@Path("userId") Long userId);

    @POST("/login")
    Call<User> login(@Body UserLoginDto userLoginDto);

    @POST("/register")
    Call<UserRegistrationDto> register(@Body UserRegistrationDto userRegistrationDto);

    @POST("/update")
    Call<User> update(@Body User user);
}
