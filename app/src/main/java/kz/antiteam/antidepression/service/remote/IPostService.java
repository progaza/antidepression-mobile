package kz.antiteam.antidepression.service.remote;

import java.util.List;

import kz.antiteam.antidepression.model.dto.NotificationDTO;
import kz.antiteam.antidepression.model.dto.PostDto;
import kz.antiteam.antidepression.model.dto.UserRegistrationDto;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface IPostService {

    @POST("/post/list/dto/{isOldPersonPosts}")
    Call<List<PostDto>> getPostDtoList(@Path("isOldPersonPosts") boolean isOldsPersonPosts);

    @POST("/post/list/my-publications/{userId}")
    Call<List<PostDto>> getPostDtoListByMe(@Path("userId") Long userId);

    @GET("/post/list/actions/{userId}")
    Call<List<NotificationDTO>> getActionsByMe(@Path("userId") Long userId);

    @POST("/post")
    Call<PostDto> createPost(@Body PostDto postDto);
}
