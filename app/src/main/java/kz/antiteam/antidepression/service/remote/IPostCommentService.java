package kz.antiteam.antidepression.service.remote;

import java.util.List;

import kz.antiteam.antidepression.model.PostComment;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface IPostCommentService {
    @GET("/post-comment/list/{postId}")
    Call<List<PostComment>> getListByPostId(@Path("postId") Long postId);

    @POST("/post-comment")
    Call<List<PostComment>> createPostComment(@Body PostComment postComment);
}
