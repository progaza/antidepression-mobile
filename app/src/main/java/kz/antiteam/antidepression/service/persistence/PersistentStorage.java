package kz.antiteam.antidepression.service.persistence;
import android.content.Context;
import android.content.SharedPreferences;

public class PersistentStorage {
    public static final String STORAGE_NAME = "Antidepression";

    private static SharedPreferences settings = null;
    private static SharedPreferences.Editor editor = null;
    private static Context context = null;

    public static void init( Context cntxt ){
        context = cntxt;
    }

    private static void init(){
        settings = context.getSharedPreferences(STORAGE_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();
    }

    public static void addProperty( String name, Integer value ){
        if( settings == null ){
            init();
        }
        editor.putInt( name, value );
        editor.apply();
    }

    public static void addProperty( String name, String value ){
        if( settings == null ){
            init();
        }
        editor.putString( name, value );
        editor.apply();
    }

    public static Integer getIntProperty( String name ){
        if( settings == null ){
            init();
        }
        return settings.getInt( name, 0 );
    }

    public static String getStringProperty( String name ){
        if( settings == null ){
            init();
        }
        return settings.getString( name, "");
    }
}