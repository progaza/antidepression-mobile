package kz.antiteam.antidepression.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;

import kz.antiteam.antidepression.model.dto.NotificationDTO;
import kz.antiteam.antidepression.util.service.DateUtils;
import kz.antiteam.antidepression.util.service.ServiceUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PostComment {
    private String text;
    @JsonProperty("userId")
    private Long userId;
    @JsonProperty("postId")
    private Long postId;
    private String userName;
    private String createDate;

    public String getLocalDate() {
        String datee = createDate;
        String ret = "15.05 12:29";
        if (!datee.endsWith("Z")) {
            datee += "Z";
        }
        if (datee.length() > 18) {
            ServiceUtil.LOG("cur : " + datee);
            int l = 5;
            if (datee.length() == 23) {
                l = 4;
            } else if (datee.length() == 27) {
                l = 8;
            }
            int until = datee.length() - l;
            ServiceUtil.LOG("cur unt: " + until);

            String substring = datee.replace("T", " ").substring(0, until);
            ret = DateUtils.stringDateToFormatString(substring, "dd.MM HH:mm");
        }
        return ret;
    }

    public PostComment(NotificationDTO notificationDTO) {
        this.text = notificationDTO.getName();
        this.userName = notificationDTO.getAuthor();
        this.createDate = notificationDTO.getCreatedDate();
    }
}
