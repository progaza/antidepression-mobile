package kz.antiteam.antidepression.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.firebase.firestore.DocumentSnapshot;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Objects;

import kz.antiteam.antidepression.util.service.DateUtils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Message implements Comparable<Message> {
    private String docId;
    private String owner;
    @JsonProperty("room_id")
    private String roomId;
    @JsonProperty("create_date")
    private String createDate;
    private LocalDateTime createdLocalDate;
    private String text;

    public Message(DocumentSnapshot documentSnapshot) {
        this.docId = documentSnapshot.getId();
        this.createdLocalDate = LocalDateTime.now();
        this.createDate = DateUtils.localDateToStringFormat(this.createdLocalDate, "dd.MM HH:mm");
        String datee = documentSnapshot.getString("create_date");
        if (!datee.endsWith("Z")) {
            datee += "Z";
        }
        if (datee != null && datee.length() > 18) {
            String substring = datee.replace("T", " ").substring(0, datee.length() - 5);

            this.createDate = DateUtils.stringDateToFormatString(substring, "dd.MM HH:mm");
            this.createdLocalDate = DateUtils.stringDateToLocalDateTime(substring);
        }
        this.text = documentSnapshot.getString("text");
        this.owner = documentSnapshot.getString("owner");
        this.roomId = documentSnapshot.getString("room_id");
    }

    public HashMap<String, Object> toHashMap() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("create_date", LocalDateTime.now().toString());
        hashMap.put("text", text);
        hashMap.put("room_id", roomId);
        hashMap.put("owner", owner);
        return hashMap;
    }

    @Override
    public int compareTo(Message o) {
        return getCreatedLocalDate().compareTo(o.getCreatedLocalDate());
    }

}
