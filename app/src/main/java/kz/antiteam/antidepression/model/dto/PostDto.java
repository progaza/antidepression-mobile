package kz.antiteam.antidepression.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class PostDto {
    @Expose
    @SerializedName("id")
    private Long id;
    @Expose
    @SerializedName("creatorUserId")
    private Long creatorUserId;
    @Expose
    @SerializedName("userName")
    private String userName;
    @Expose
    @SerializedName("userAssetId")
    private Long userAssetId;
    @Expose
    @SerializedName("assetId")
    private Long assetId;
    @Expose
    @SerializedName("title")
    private String title;
    @Expose
    @SerializedName("body")
    private String body;
    @Expose
    @SerializedName("createDate")
    private String createDate;
    @Expose
    @SerializedName("commentCount")
    private Long commentCount;
}
