package kz.antiteam.antidepression.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class NotificationDTO {
    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("author")
    private String author;
    @Expose
    @SerializedName("createdDate")
    private String createdDate;
}
