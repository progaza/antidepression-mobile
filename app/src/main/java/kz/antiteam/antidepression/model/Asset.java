package kz.antiteam.antidepression.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Asset {

    @Expose
    @SerializedName("id")
    private Long id;

    @Expose
    @SerializedName("hash")
    private String hash;

}
