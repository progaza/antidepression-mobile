package kz.antiteam.antidepression.model;

import androidx.navigation.NavType;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import kz.antiteam.antidepression.util.constant.Gender;
import kz.antiteam.antidepression.util.constant.Status;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {
    @Expose
    @SerializedName("id")
    private Long id;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("email")
    private String email;

    @Expose
    @SerializedName("age")
    private Integer age;

    @Expose
    @SerializedName("gender")
    private Gender gender;

    @Expose
    @SerializedName("about")
    private String about;

    @Expose
    @SerializedName("password")
    private String password;

    @Expose
    @SerializedName("confirmationCode")
    private String confirmationCode;

    @Builder.Default
    @Expose
    @SerializedName("status")
    private Status status;
}
