package kz.antiteam.antidepression.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import kz.antiteam.antidepression.util.constant.Gender;
import lombok.Data;

@Data
public class UserRegistrationDto {

    @Expose
    @SerializedName("id")
    private Integer id;

    @Expose
    @SerializedName("email")
    private String email;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("age")
    private Integer age;

    @Expose
    @SerializedName("gender")
    private Gender gender = Gender.MALE;

    @Expose
    @SerializedName("about")
    private String about;

    @Expose
    @SerializedName("password")
    private String password;

    public Integer getId() {
        return id;
    }

    public UserRegistrationDto(String email, String name, Integer age, Gender gender, String about, String password) {
        this.email = email;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.email = email;
        this.about = about;
        this.password = password;
    }
}
