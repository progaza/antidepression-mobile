package kz.antiteam.antidepression.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.gson.annotations.Expose;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Objects;

import kz.antiteam.antidepression.util.service.DateUtils;
import kz.antiteam.antidepression.util.service.ServiceUtil;

public class Room implements Comparable<Room> {

    @JsonProperty("doc_id")
    private String docId;

    @JsonProperty("create_date")
    private String createDate;
    @JsonProperty("last_message")
    private String lastMessage;
    private String owner;
    private String participant;
    @Expose
    @JsonProperty("post_id")
    private Integer postId;

    private LocalDateTime createdLocalDate;

    public Room(DocumentSnapshot documentSnapshot) {
        this.docId = documentSnapshot.getId();
        this.createdLocalDate = LocalDateTime.now();
        this.createDate = DateUtils.localDateToStringFormat(this.createdLocalDate, "dd.MM HH:mm");
        String datee = documentSnapshot.getString("create_date");
        if (!datee.endsWith("Z")) {
            datee += "Z";
        }
        if (datee != null && datee.length() > 18) {
            String substring = datee.replace("T", " ").substring(0, datee.length() - 5);
            this.createDate = DateUtils.stringDateToFormatString(substring, "dd.MM HH:mm");
            this.createdLocalDate = DateUtils.stringDateToLocalDateTime(substring);
        }
        this.lastMessage = documentSnapshot.getString("last_message");
        this.owner = documentSnapshot.getString("owner");
        this.participant = documentSnapshot.getString("participant");
        this.postId = !Objects.isNull(documentSnapshot.getLong("post_id")) ? documentSnapshot.getLong("post_id").intValue() : 0;
    }

    public Room() {
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getParticipant() {
        return participant;
    }

    public void setParticipant(String participant) {
        this.participant = participant;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public LocalDateTime getCreatedLocalDate() {
        return createdLocalDate;
    }

    public void setCreatedLocalDate(LocalDateTime createdLocalDate) {
        this.createdLocalDate = createdLocalDate;
    }

    public HashMap<String, Object> toHashMap(String lastMessage2) {
        HashMap<String, Object> toHash = new HashMap<>();
        toHash.put("last_message", lastMessage2);
        toHash.put("post_id", postId);
        toHash.put("owner", owner);
        toHash.put("participant", participant);
        toHash.put("create_date", LocalDateTime.now().toString());
        return toHash;
    }

    @Override
    public int compareTo(Room o) {
        return getCreatedLocalDate().compareTo(o.getCreatedLocalDate());
    }
}

