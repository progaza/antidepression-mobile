package kz.antiteam.antidepression.util.constant;

public enum Gender {
    MALE,
    FEMALE
}
