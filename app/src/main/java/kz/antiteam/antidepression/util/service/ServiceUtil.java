package kz.antiteam.antidepression.util.service;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;


public class ServiceUtil {
    public static final String TAG = "AntiDepressionApplication";

    public static void showMessage(Context context, String message){
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void LOG(String message){
        Log.i(TAG, "\n");
        Log.i(TAG, "\n_____________________________\n");
        Log.i(TAG, message);
        Log.i(TAG, "\n_____________________________\n");
    }

    public static String toShortText(String text){
        if(text.length() > 20){
            text = text.substring(0, 20) + "...";
        }
        return text;
    }

    public static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
}
