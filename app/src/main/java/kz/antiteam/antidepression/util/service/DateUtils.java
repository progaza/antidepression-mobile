package kz.antiteam.antidepression.util.service;


import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class DateUtils {
    private static final DateTimeFormatter LOCAL_DATE_FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    private static final DateTimeFormatter LOCAL_DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd:MM:yyyy HH:mm:ss");
    public static final String ISO_DATE_TIME_FORMAT_STRING = "yyyy-MM-dd HH:mm:ss";
    public static final String ISO_DATE_FORMAT = "yyyy-MM-dd";
    public static final String DATE_FORMAT = "dd.MM.yyyy";
    public static final String DATE_TIME_FORMAT = "dd.MM.yyyy HH:mm";
    public static final String DATE_TIME_SECONDS_FORMAT = "dd.MM.yyyy HH:mm:ss";
    public static final String DATE_TIME_FORMAT_EXTENDED = "dd MMMM yyyy, HH:mm";

    public static String localDateToStringFormat(LocalDateTime localDateTime, String stringFormat) {
        return localDateTime.format(DateTimeFormatter.ofPattern(stringFormat));
    }

    public static LocalDateTime timestampToLocalDateTimeFormat(Long timestamp) {
        return Instant.ofEpochMilli(timestamp).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static String timestampToStringFormat(Long timestamp) {
        return localDateToStringFormat(timestampToLocalDateTimeFormat(timestamp), DATE_TIME_SECONDS_FORMAT);
    }

    public static String stringDateToFormatString(String date, String stringFormat) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(date, formatter);
        return localDateToStringFormat(dateTime, stringFormat);
    }

    public static LocalDateTime stringDateToLocalDateTime(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return LocalDateTime.parse(date, formatter);
    }
}
