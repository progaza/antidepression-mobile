package kz.antiteam.antidepression.util.service;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import android.widget.ImageView;

import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;


import org.apache.commons.io.IOUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ImageUtils {

    private static final float MAX_IMAGE_SIZE = 1366;

    public static void copyImage(Context context, Uri sourceUri, Uri targetUri) {
        Bitmap bitmap;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), sourceUri);
            Matrix matrix = new Matrix();
            Bitmap bmp = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            //learn content provider for more info
            OutputStream os = context.getContentResolver().openOutputStream(targetUri);
            bmp.compress(Bitmap.CompressFormat.JPEG, 90, os);
            os.flush();
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void compress(InputStream inputStream, OutputStream out) {
        try (ByteArrayOutputStream bout = new ByteArrayOutputStream();) {
            IOUtils.copy(inputStream, bout);
            byte[] bytes = bout.toByteArray();

//            try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes)) {
//                Metadata metaData = ImageMetadataReader.readMetadata(byteArrayInputStream);
//            } catch (ImageProcessingException e) {
//                e.printStackTrace();
//            }
            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            Bitmap scaledBitmap = scaleDown(bitmap, MAX_IMAGE_SIZE, true);
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            scaledBitmap.recycle();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                                   boolean filter) {
        float ratio = Math.min(
                maxImageSize / realImage.getWidth(),
                maxImageSize / realImage.getHeight());
        int width = Math.round(ratio * realImage.getWidth());
        int height = Math.round(ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }

    public static void saveBitmap(Context context, Bitmap bitmap, Uri targetUri) {
        try {
            Matrix matrix = new Matrix();
            Bitmap bmp = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            //learn content provider for more info
            OutputStream os = context.getContentResolver().openOutputStream(targetUri);
            bmp.compress(Bitmap.CompressFormat.JPEG, 90, os);
            os.flush();
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

